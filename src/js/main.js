window.jQuery = window.$ = require('jquery');

// DOM ready
(function() {
    // js support flag
    let doc = document.documentElement;
    doc.classList.remove('no-js');
    doc.classList.add('js');
}());

