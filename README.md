# BuildFront v2.1

フロントエンド開発用のタスクランナーセット

## 特徴

- ローカルサーバー環境と自動リロード
- postcss-preset-envを使った次世代のCSS記述をCSS3に変換＆圧縮
- ES6の記述をES5に変換＆圧縮＆難読化
- 画像の自動最適化（jpg、png、svg）

## 含まれるもの

- [sanitize.css](https://jonathantneal.github.io/sanitize.css/)
- [Basscss](http://basscss.com/)
- [jquery](https://jquery.com/)

## 必要な環境

- OSX
- Node.js
- npm

## 使用方法

###インストール

ターミナルでプロジェクトフォルダに移動し、
```
npm install
```
※インストール後に自動でサーバーが立ち上がり、監視モードに入ります。

### サーバー立ち上げ＆ファイル監視
```
npm run watch
```
Ctrl + C で終了。

### 公開用ファイルをビルド
```
npm run build
```
※ 監視モード下のビルドと異なりソースマップが吐き出されません。
