module.exports = ctx => ({
    map: ctx.options.map,
    plugins: {
        'postcss-import': {},
        'postcss-preset-env': {
            browsers: 'last 2 versions',
            stage: 0,
        },
        'postcss-custom-properties': {
            preserve: false,
        },
        'cssnano': ctx.env === 'production' ? {} : false
    },
})
